from django.apps import AppConfig


class QuiezzesConfig(AppConfig):
    name = 'quiezzes'
