from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect,get_object_or_404
from django.contrib.auth import authenticate, login, get_user_model,logout,login
from django.core.urlresolvers import reverse
from .models import Quizz
from .models import Questions
from quiezzes.forms import UserForm


def index(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login'))
    quizzes = Quizz.objects.all()
    return render(request, 'quiezzes/quizzes.html', {'username':request.user.username,'quizzes': quizzes})

@login_required(login_url="/login/")
def go_quizz(request,pk):
    quizz = Questions.objects.filter(quizz=pk)
    return render(request, 'quiezzes/quizz.html', {'username':request.user.username,'quizz':quizz,'pk':pk})

def check(request):
    if request.method == 'POST':
        answers = request.POST
        pk = answers.get('pk')
        quizz = Questions.objects.filter(quizz=pk)
        right_answer = []
        id = []
        for ans in quizz:
            right_answer.append(ans.right_answer)
            id.append(ans.id)
        score = 0
        for i in range(0,quizz.count()):
            if (answers.get('{}'.format(id[i]))) == right_answer[i]:
                # print('True',answers.get('{}'.format(id[i])),right_answer[i])
                score +=1
            # else:
                # print('False',answers.get('{}'.format(id[i])),right_answer[i])
        return render(request, 'quiezzes/results.html', {'username':request.user.username,'score':score})
    return render(request, 'quiezzes/results.html', {'username':request.user.username,})

def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            registered = True
    else:
        user_form = UserForm()
    return render(request,
                  'quiezzes/register.html',
                  {'user_form': user_form,

                   'registered': registered
                   })

@login_required(login_url="/login/")
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse("Your Rango account is disabled.")
        else:
            print("Invalid login details: {0}, {1}".format(username, password))
            return HttpResponse("Invalid login details supplied.")
    elif request.method == 'GET':
        return render(request, 'quiezzes/login.html', {})
