from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^check/$', views.check, name='check'),
    url(r'^quizz/(?P<pk>[0-9]+)/$', views.go_quizz, name='go_quizz'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
]