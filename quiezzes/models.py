from django.db import models

class Quizz(models.Model):
    title = models.CharField(max_length = 200, blank=True)

    def __str__(self):
        return self.title

class Questions(models.Model):
    quizz = models.ForeignKey(Quizz, on_delete = models.CASCADE)
    question = models.CharField(max_length=1000, blank=True)
    right_answer = models.CharField(max_length=200, blank=True)
    answer1 = models.CharField(max_length=200, blank=True)
    answer2 = models.CharField(max_length=200, blank=True)
    answer3 = models.CharField(max_length=200, blank=True)
    answer4 = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.question